#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

#define BIT1 0x01
#define BIT2 0x02
#define BIT3 0x04
#define BIT4 0x08
#define BIT5 0x10
#define BIT6 0x20
#define BIT7 0x40
#define BIT8 0x80
#define BASE_16 16


void parse_amd_pdu(char *buffer);
char *get_pure_hex_buffer(FILE *file);

int main(int argc, char **argv)
{
    if (argc == 1){
        printf("No args.\n");
        return 1;
    }
    FILE *file = fopen(argv[1], "r");
    if (file == NULL){
        printf("No such file.\n");
        return 1;
    }
    char *buffer = get_pure_hex_buffer(file);
    fclose(file);
    printf("%s\n", buffer);
    parse_amd_pdu(buffer);
}

char *get_pure_hex_buffer(FILE *file){
    // Get the file size and read it into a memory buffer
	fseek(file, 0L, SEEK_END);  // ustaw wskaźnik na plik na koniec pliku
	size_t fsize = ftell(file); // Sprawdź na którym bajcie pliku jesteś (czyli sprawdz rozmiar pliku)
    fseek(file, 0L, SEEK_SET);  // ustaw wskaźnik z powrotem na początek pliku

    char *buffer = malloc(fsize * sizeof(char));    // alokuj bufor na cały plik
    fread(buffer, fsize, 1, file);  // wczytaj cały plik do bufora
    char c = buffer[0]; 
    int i;
    int digitCount = 0;
    
    // Tutaj przepisuje wszystkie znaki do bufora pomijając spacje 
    // i entery

    for (i = 0; i < fsize; i++){
        if (isalnum(c))
            digitCount++;
        c = buffer[i + 1];
    }   

    char *hexBuffer = malloc(digitCount * sizeof(char) + 1);
    c = buffer[0];   
    int hexBuffIndex = 0; 
    for (i = 0; i < fsize; i++){
        if (isalnum(c)){
            hexBuffer[hexBuffIndex] = c;
            hexBuffIndex++;
        }
        c = buffer[i + 1];
    }
    hexBuffer[hexBuffIndex] = '\0';
    free(buffer);
    return hexBuffer;
}

void parse_amd_pdu(char *buffer){
    char headerByte1[3];
    char headerByte2[3];
    memcpy(headerByte1, &buffer[0], 2);
    memcpy(headerByte2, &buffer[2], 2);
    headerByte1[2] = '\0';
    headerByte2[2] = '\0';
    long int oct1;
    long int oct2;

    /*
    for (int i = 0; i < strlen(buffer); i+=2){
        memcpy(headerByte1, &buffer[i], 2);
        headerByte1[2] = '\0';
        oct1 = strtol(headerByte1, NULL, BASE_16);
        printf("%s hex: %02x\n", headerByte1, (unsigned int)oct1);
    }
    */
    
    oct1 = strtol(headerByte1, NULL, BASE_16);
    oct2 = strtol(headerByte2, NULL, BASE_16);
    printf ("oct1: %02x\n", (unsigned int)oct1);
    printf ("oct2: %02x\n", (unsigned int)oct2);

    int dc = ((oct1 & BIT8) != 0);
    int rf = ((oct1 & BIT7) != 0);
    int p  = ((oct1 & BIT6) != 0);
    int fi = ((oct1 & (BIT5 | BIT4)) >> 3);
    int e  = ((oct1 & BIT3) != 0);
    int sn = ( ((oct1 & (BIT1 | BIT2)) << 8) | oct2);

    printf("dc = %d rf = %d p = %d fi = %d e = %d sn = 0x%04x\n", dc, rf, p, fi, e, sn);

    char *dataPointer = NULL;
    if (e == 1){

    }
    else{
        dataPointer = &buffer[4];
    }
    printf("Data:\n");
    while ( (*dataPointer) != '\0'){
        printf("%c", *dataPointer);
        dataPointer++;
    }
    printf("\n");
}
